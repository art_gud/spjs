$(document).ready(function(){
    var formButtons = $('#user-registration .btn');

    var action = function(type){
        switch(type){
            case 'register':
                postUser();
                break;
            case 'list':
                getUser();
                break;
        }
        return false;
    }

    $(formButtons).on('click', function(e){
        action($(this).attr('data-buttonact'));
        e.preventDefault();
    });

    var postUser = function(){
        sendResponse('POST', getRegisterFormData);
    }

    var getRegisterFormData = function(){
        var data = {};
        var ev = $('#email').val();
        var pv = $('#password').val();
        data = {
            email: ev,
            password: pv
        }
        return data;
    }

    var getUser = function(){
        sendResponse('GET');
    }

    var sendResponse = function(type, data){
        if(!data)
            var ajaxData = {};
        var ajaxDataType = 'jsonp';
        switch(type){
            case 'POST':
                ajaxData = data();
                ajaxDataType = 'json';
                break;
            case 'GET':
                ajaxData = { user_id: 1 };
                break;
            case 'PUT':
                break;
            case 'DELETE':
                break;
        }

        $.ajax({
            url: 'http://spjs.izotov.tk/api/users',
            dataType: ajaxDataType,
            crossDomain: true,
            type: type,
            data: ajaxData,
            }).then(function(data){
                if(data.error){}
                else {
                    renderResults(data);
                }
        });

        return false;
    }
    
    var renderResults = function (resp){
        if($.isArray(resp.Users)){
            var userWrapper = $('#user-registration');
            var userBox = $('<div></div>');
            $.each(resp.Users, function(){
                var userBoxWithData = $(userBox).clone();
                $(userBoxWithData).html('<a href="#">'+this.user_email+'</a>');
                $(userWrapper).append($(userBoxWithData));
            });

        }
    }
    
    $(".form-horizontal").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                min: 4
            },
            password_confirm: {
                required: true,
                equalTo: "#password"
            }

        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element.form).find("label[for=" + element.id + "]")
                .addClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element.form).find("label[for=" + element.id + "]")
                .removeClass(errorClass);
        }
    });

});