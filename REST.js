var mysql   = require("mysql");

function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes = function(router,connection,md5) {
    var self = this;
    router.get("/",function(req,res){
        res.json({"Message" : "Hello World !"});
    });

    router.get("/users",function(req,res){
        var query = "SELECT * FROM ??";
        var table = ["user_login"];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            if(err) {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : true, "Message" : "Error executing MySQL query"}) + ');');
            } else {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : false, "Message" : "Success", "Users" : rows}) + ');');
            }
        });
    });

    router.get("/users/:user_id",function(req,res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["user_login","user_id",req.params.user_id];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            if(err) {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : true, "Message" : "Error executing MySQL query"}) + ');');
            } else {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : false, "Message" : "Success", "Users" : rows}) + ');');
            }
        });
    });

    router.post("/users",function(req,res){
        var query = "INSERT INTO ??(??,??) VALUES (?,?)";
        var table = ["user_login","user_email","user_password",req.body.email,md5(req.body.password)];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.header('Access-Control-Allow-Origin', 'http://localhost:63342')
            if(err) {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : true, "Message" : "Error executing MySQL query"}) + ');');
            } else {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : false, "Message" : "User added!"}) + ');');
            }
        });
    });

    router.put("/users",function(req,res){
        var query = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
        var table = ["user_login","user_password",md5(req.body.password),"user_email",req.body.email];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            if(err) {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : true, "Message" : "Error executing MySQL query"}) + ');');
            } else {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : false, "Message" : "Updated the password for email "+req.body.email}) + ');');
            }
        });
    });

    router.delete("/users/:email",function(req,res){
        var query = "DELETE from ?? WHERE ??=?";
        var table = ["user_login","user_email",req.params.email];
        query = mysql.format(query,table);
        connection.query(query,function(err,rows){
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            if(err) {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : true, "Message" : "Error executing MySQL query"}) + ');');
            } else {
                res.send(req.query.callback + '('+ JSON.stringify({"Error" : false, "Message" : "Deleted the user with email "+req.params.email}) + ');');
            }
        });
    });
}

module.exports = REST_ROUTER;
